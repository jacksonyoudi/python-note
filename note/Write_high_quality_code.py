3. 使用collections.namedtuple
from collections import namedtuple
seasons = namedtuple('Seasons','Spring Summer Autumn Winter')._make(range(4))

4.使用第三方的模块
from flufl.enum import Enum
class Seasons(Enum):
    Spring = "Spring"
    Summer = 2
    Autumn = 3
    Winter = 4

####12: 不推荐使用type来进行类型检查
隐式类型转换
不能进行类型转换的会抛出　TypeError
```
import types
if type(a) is types.ListType:
```
原因１：　
基于内建类型扩展的用户自定义类型，type函数并不能准确返回结果

原因２：
在经典类中，任意类的实例的type()返回结果都是<type 'instance'>，可以使用isinstance()


####13: 尽量转换为浮点类型后再做除法
from __future__ import division

####14: 警惕eval()的安全漏洞
```
eval(source[, globals[, locals]]) -> value
```
globals,locals分别表示全局和局部命名空间
防止,命令的注入

####15:使用enumerate()获取序列迭代的索引和值
```
li = [1,2,3,4,5,6]
for index,value in enumerate(li):
    print index,value
```
enumerate()参数可以是 list,dict,iterato　或其他任何可以迭代的对象，默认开始是start为0,
函数返回本质上为一个迭代器，可以使用next()方法获取下一个迭代元素
```
li = [1,2,3,4,5]
e = enumerate(li)
e.next()

enumerate(li,start=10)
```


####30: [],()和{}：一致性的容器初始化形式
**列表解析**：　(list comprehension)
[expr for iter_item in iterable if cond_expr]

1)支持多重嵌套
nested_list = [['hello','world'],['goodbye','world']]
nested_list = [[s.upper() for s in xs] for xs in nested_list]
[['HELLO', 'WORLD'], ['GOODBYE', 'WORLD']]

2)支持多重迭代
[(a,b) for a in ['a','1',1,2] for b in ['1',3,4,'b'] if a != b]
[('a', '1'),('a', 3),('a', 4),('a', 'b'),('1', 3),('1', 4),('1', 'b'),(1, '1'),(1, 3),(1, 4),(1, 'b'),(2, '1'), (2, 3),(2, 4),(2, 'b')]

3)列表解析语法中的表达式可以是简单的表达式,也可以是复杂的表达式
def f(v):
    if v%2 == 0:
        v = v ** 2
    else:
        v = v + 1
    return v
[f(v) for v in [2,3,4,5,6,-1] if v>0]
[v**2 if v%2 == 0 else v+1 for v in [2,3,4,5,6,-1] if v > 0]

4)列表解析语法中的iterable可以是任意可迭代对象
fh = open('test.txt','r')
result  = [i for i in fh if "abc" in i]

**使用列表解析优点**
- 使用列表解析简洁清晰
- 效率更高

**提示**
tuple,set,dict　都有相应的解析方式
type((1)) type((1,))

####31: 记住函数传参既不是传值也不是引用
传对象(call by object)或传对象引用(call-by-object-refernce)
函数参数在传递过程中将对象传入，对于可变对象的修改在函数外部以及内部是可见的，调用者和被调用者之间共享这个对象，对于不可变对象，由于并不能真正被修改，因此，修改往往是通过生成一
新的对象然后赋值来实现的．

####32:警惕默认参数潜在的问题
def appendtest(newitem,lista=[]):
    print id(lista)
    lista.append(newitem)
    print id(lista)
    return lista

appendtest(1)
appendtest(2)

appendtest.func_defaults 会保存默认值

**提示**
如果不想让默认参数所指向对象在所有的函数中调用中被共享，而是在函数调用的过程中动态　生成，可以在定义的时候使用None对象作为占位符．

####33: 慎用变长参数
1)使用*args来实现可变参数列表，*args用于接受一个包装成元祖形式的参数列表来传递非关键字参数，参数个数可以任意．
2)使用**kwargs接受字典形式的关键字参数列表，其中字典的键值对分别表示不可变参数的参数名和值．

使用场景：
* 函数添加一个装饰器
def mydecorator(fun):
    def new(*args,**kwargs):
        # ..
        return fun(*args,**kwargs)
    return new

* 如果参数数目不确定，可以考虑使用变长参数
文件　test.cfg
[Defaults]
name = test
version = 1.0
platform  = linux

from ConfigParser import ConfigParser
conf = ConfigParser()
conf.read('test.cfg')
conf_dict = dict(conf.items('Defaults'))
def func(**kwargs):
    kwargs.update(conf_dict)
    global name
    name = kwargs.get('name')
    global version
    version = kwargs.get('version')
    global platform
    platform = kwargs.get('platform')

* 用来事项函数的多态或者在继承情况下子类需要调用父类的某些方法的时候．
class A(object):
    def somefun(self,p1,p2):
        pass

class B(A):
    def myfun(self,p3,*args,**kwargs):
        super(B,self).somefun(*args,**kwargs)


####34:深入理解str()和rep()的区别
1)两者之间的目标不同，str()主要面向用户，其目的可读性，返回形式为用户友好性和可读性
都较强字符串类型；而repr()面向的是python解释器，或者说是开发人员，其目的就是准确性，返回值表示
python解释器内部的含义，常作为编程人员debug用途．
2)在解释器中直接输入a时默认调用repr()函数，而print a则是调用str()函数．
3)repr()的返回值一般可以用eval()函数来还原对象
obj = eval(repr(obj))
4)这两个方法分别内建的 __str__()　和 __repr__()方法，一般来说
在类中应该定义 __repr__(),而__str__()为可选．　可以evel()还原

####35:分清staticmethod和classmethod的适用场景
python中的静态方法(staticmethod)和类方法(classmethod)都依赖于装饰器(decorator)
实现．
class C(object):
    @staticmethod
    def f(cls,*args,**kwargs):
        pass
a = C()
静态方法可以使用 类名.方法名(C.f())　或者 实例.方法名(a.f())的形式来访问
其中静态方法没有常规的方法的特殊行为，如绑定，非绑定，隐式参数等规则，而类方法的调用
使用类本身作为其隐含参数，但调用本身并不需要显示提供该参数．

class Fruit(object):
    total = 0
    @classmethod
    def print_total(cls):
        print cls.total
        # print id(Fruit.total)
        # print id(cls.total)

    @classmethod
    def set(cls, value):
        # print "calling class_method(%s,%s)" %(cls,value)
        cls.total = value


class Apple(Fruit):
    pass


class Orange(Fruit):
    pass

app1 = Apple()
app1.set(200)
app2 = Apple()
org1 = Orange()
org1.set(300)
app1.print_total() # 200
org1.print_total() # 300



###第四章　库

####36: 掌握字符串的基本用法
s = ('SELECT * FROM'
     'atble WHERE'
     'a = 1')
利用python遇到未闭合的小括号时会自动将多行代码拼接为一行和把相邻的两个字符串字面量拼接在一起．
a = 'hi'
b = u'hi'
isinstance(a,basestring)
isinstance(a,str)
isinstance(b,unicode)


####37: 按需选择sort()或者sorted()
1)相比于 sort(),sorted()使用更广泛，使用如下：
sorted(iterable=a,cmp=None,key=None,reverse=None)
s.sort([cmp[,key,[reverse]]])

共同的函数：
- cmp为用户定义的任何比较函数，函数的参数为两个可以比较的元素，函数根据第一个参数和第二个参数
- key是带一个参数的函数，用来为每个元素提取比较值，默认为 None
- reverse表示是否翻转

sorted() 作用于任意可迭代的对象，不会作用原对象
sort()一般只作用于列表，对象会修改，会修改原对象

* 对字典进行排序
* 多维list排序
* 字典中混合list排序
* list中混合字典排序


####38: 使用copy模块深拷贝对象

* 浅拷贝(shallow copy):构造一个新的复合对象并将从原对象中发现的引用插入对象中
 浅拷贝的实现方式有多种，如工厂函数,切片操作，,copy模块中copy操作
* 深拷贝(deep copy):也就是构造一个新对象，但是遇见引用会继续递归拷贝其所指向的具体内容．

####39: 使用Counter进行计数统计

from collections import Counter
some_data = ['a','2','3','4','5','5','10','a','b']
print Counter(some_data)

Counter属于字典类的子类，是一个容器的对象，主要用来统计散列的对象，支持集合的操作+,-,&,|,分别返回两个Counter对象元素的
最大值和最小值.

3种方式进行初始化
Counter('Sucesser')  #可迭代对象
Counter(s=3,c=2,e=1,u=2) #关键字参数
Counter({"s":3,"c":2,"u":1,"e":1}) #字典

可以使用elements()方法获取Counter中的key值
list(Counter(some_data).elements())

利用most_common()方法可以找出前N个出现频率最高的元素以及它们对应的次数.
Counter(some_data).most_comom(2)


####40: 深入掌握ConfigParser


ConfigParser
getboolean
提示: 当需要注意的配置项的查找规则.首先,在ConfigParser支持的配置文件配置文件格式中 [DEFAULT] 中查找

import ConfigParser
conf = ConfigParser.ConfigParser()
conf.read('example.conf')
print conf.get('section1','in_default')

配置项的键值查找机制规则如下:
1) 如果找不到节名,就会抛出NoSectionError
2) 如果给定的配置项出现在get()方法的vars参数中,则返回vars参数的值
3) 如果在指定的的节中含有给定的配置项,则返回值
4) 如果在[DEFAULT]中有指定的配置项,则返回其值
5) 如果在构造器函数中的default参数中有指定配置项,则返回值
6) 抛出NoOptionError


####41: 使用argparse处理命令行参数

from optparse import OptionParser

parser = OptionParser()
parser.add_option(
    "-f","--file",dest='filename',
    help="write report to FILE",metavar="FILE"
)
parser.add_option(
    "-q","--quiet",action='store_false',dest="verbose",deafult=True,
    help="don't print status messages to stdout"
)

(options,args) = parser.parse_args()


import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-o",'--output')
parser.add_argument("-v",dest="verbose",action='store_true')
args = parser.parse_args()


####42:使用pandas处理大型cvs文件

reader(csvfile[,dialect="excel"][,fmtparam])
主要用于csv文件的读取,返回一个reader对象用于在csv文件的读取,返回一个reader对象用于
在csv文件内容上进行迭代.


import csv
csv.writer(cvsfile,dialect='excel',**fmtparamas)

with open('data.csv','wb') as csvfile:
    csvwrite = csv.writer(cvsfile,dialect='excel',delimiter="|",quotechar='"',quoting=csv.QUOTE_MINIMAL)
    csvwrite.writerow([["111,33,333,4,55,lll"]])


Pandas即Python Data Analysis Library,是为了解决数据分析而创建的第三方工具，不仅提供了丰富的数据模型，而且支持多种文件格式处理，包括cvs
HDF5,HTML等,支持两种数据结构--Series和DataFrame.
* Series:它是一种类似数组的带索引的一维数据结构，支持类型和numpy兼容．如果不指定索引，默认为0到N-1
通过 obj.values()　和 obj.index()　可以分别获取值和索引，当Series传递一个字典的时候，　Series的索引将
根据字典的时候，Series的索引将根据字典中的键排序．如果传入字典的时候同时指定index参数，当index与字典不匹配的时候，会
出现数据丢失的情况，标记为NaN

* DataFrame: 类似电子表格,DataFrame也可以通过columns指定序列顺序进行排序．


####43: 一般情况使用ElementTree解析XML

import xml

xml.dom.minidom 和　xml.sax 大概是 python中解析　XMl文件最为人知的两个模块

import cElementTree　as ElementTree

ElementTree 特性：
1. 使用简单
2. 内存上消耗低
3. 支持Xpath查询


####44: 理解模块 pickle优劣













